import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Widget from './digitalSing/widget'

ReactDOM.render(
  <React.StrictMode>
    <Widget />
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
