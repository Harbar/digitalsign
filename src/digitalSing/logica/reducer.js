import { Map } from 'immutable';
import logicaActions from './constants';

/**
 * @desc Reducer provides info about sessionOptions
 * @func
 * @param state {Immutable.Map}
 * @param action {object}
 * @param action.type {string}
 * @param action.payload {{token:{string},sessionOptions:{object}}}
 * @return {Immutable.Map}
 */
const logicaReducer = (state = new Map(), { type, payload }) => {
  if (!type.startsWith('@logica')) {
    // Не наш узел
    return state;
  }

  switch (type) {
  case logicaActions.getIdLogica:
    return state
      .set('requestId', payload.requestId)
      .set('stringToSign', payload.stringToSign)
      .set('errorCode', payload.errorCode)
      .set('errorDescription', payload.errorDescription);
  case logicaActions.loginLogica:
    return state
      .set('success', payload.success)
      .set('ticket', payload.ticket)
      .set('organizationId', payload.organizationId)
      .set('organizationName', payload.organizationName)
      .set('registrationNeeded', payload.registrationNeeded)
      .set('errorCode', payload.errorCode)
      .set('errorMessage', payload.errorMessage)
      .set('errorDescription', payload.errorDescription);
  case logicaActions.readPrivateKey:
    return state
      .set('subjFullName', payload.subjFullName)
      .set('subjOrg', payload.subjOrg)
      .set('subjDRFOCode', payload.subjDRFOCode)
      .set('subjEDRPOUCode', payload.subjEDRPOUCode);
  default:
    return state;
  }
};

export default logicaReducer;
