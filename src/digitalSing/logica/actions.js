import logicaActions from './constants';
import { apiLogica } from './api';


const loginLogica = (requestId, signBase64) => (dispatch) => {
  const body = {
    signBase64,
    userAgent: 'Test',
  };

  let resReq = {};
  (async () => {
    const response = await apiLogica.authentication.login({ requestId, body });
    if (response.ok) {
      const payload = await response.json();
      resReq = payload;
      dispatch({
        type: logicaActions.loginLogica,
        payload,
      });
    } else {
      console.log('response', response);
    }
  })();
  return resReq;
};

const getIdLogica = async () => {
  const responseRequestId = await apiLogica.authentication.getIdForSign();
  if (responseRequestId.ok) {
    const { requestId, stringToSign } = await responseRequestId.json();
    return { requestId, stringToSign };
  }
  console.log(responseRequestId);
  return null;
};

const readPrivateKey = ({
  subjFullName,
  subjOrg,
  subjDRFOCode,
  subjEDRPOUCode,
}) => (dispatch) => {
  console.log('action readPrivateKey');
  dispatch({
    type: logicaActions.readPrivateKey,
    payload: {
      subjFullName,
      subjOrg,
      subjDRFOCode,
      subjEDRPOUCode,
    },
  });
};


export default {
  getIdLogica,
  readPrivateKey,
  loginLogica,
};

