const logicaActions = {
  readPrivateKey: '@logica/readPrivateKey',
  getIdLogica: '@logica/getIdForSign',
  loginLogica: '@logica/loginLogica',
};

export default logicaActions;
