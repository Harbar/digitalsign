// import api from '../../../../api/req';
// import logicaActions from './actions';

const reqParams = body => (
  {
    headers: {
      accept: 'application/json',
      // 'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
    method: 'POST',
  }
);


const base = 'https://logica.minfin.gov.ua/ws/api';
// const baseFake = 'https://logica.min1fin.g1ov.ua/ws/api/';
const baseBackEnd = 'https://srv52.ciat.net.ua/BM_H/hs';
// const base = 'https://test.ua/';

export const apiLogica = {
  authentication: {
    getIdForSign: async () => await fetch(`${base}/_MINFIN.GET_ID_FOR_SIGN`),
    login: async ({ requestId, body }) => await fetch(`${base}/_MINFIN.LOGIN?requestId="${requestId}"`, reqParams(body)),
    backEnd: async body => await fetch(`${baseBackEnd}/logica/login`, reqParams(body)),
  },

};

// 3a0a62a9-de1f-fe01-e053-0a1e06c63932

// curl -X POST "https://logica.minfin.gov.ua/ws/api/_MINFIN.LOGIN?requestId=79e35ca9-2cdf-f601-e053-0a1e06c631fd" -H "accept: application/json" -H "Content-Type: application/json" -d "\"string\""

// fetch("https://logica.minfin.gov.ua/ws/api/_MINFIN.LOGIN?requestId=%22531561a9-0aaf-5c01-e053-0a1e06c6e6dd%22", {
//   "headers": {
//     "accept": "application/json",
//     "accept-language": "en-US,en;q=0.9",
//     "content-type": "application/json",
//     "sec-fetch-dest": "empty",
//     "sec-fetch-mode": "cors",
//     "sec-fetch-site": "same-origin"
//   },
//   "referrer": "https://logica.minfin.gov.ua/ws/swagger/",
//   "referrerPolicy": "no-referrer-when-downgrade",
//   "body": "\"Test\"",
//   "method": "POST",
//   "mode": "cors",
//   "credentials": "omit"
// });
// 'sec-fetch-dest': 'empty',
// 'sec-fetch-mode': 'cors',
// 'sec-fetch-site': 'same-origin',
// 'accept-language': 'en-US,en;q=0.9',
//     mode: 'no-cors',
//     credentials: 'omit',
