import React, { useEffect, useRef, useState } from 'react';
// import { connect } from 'react-redux';
// import { Map } from 'immutable';
import init from './eusign';

import {Textarea, Button} from '../components';
import { apiLogica } from '../logica/api';
// import actions from '../logica/actions';
import logicaActions from '../logica/actions';
//= ================================================================
// Ідентифікатор батківського елементу для відображення iframe,
// який завантажує сторінку SignWidget

const SIGN_WIDGET_PARENT_ID = 'sign-widget-parent';
// Ідентифікатор iframe, який завантажує сторінку SignWidget

const SIGN_WIDGET_ID = 'sign-widget';


// URI з адресою за якою розташована сторінка SignWidget
const SIGN_WIDGET_URI = 'https://eu.iit.com.ua/sign-widget/v20200518/';

// const { endUser } = init();


const Widget = () => {
  const EndUserRef = useRef(null);
  const EuSignRef = useRef(null);

  const [dataSign, setDataSign] = useState('');
  const [signBase, setSignBase] = useState('');


  useEffect(() => {
    EndUserRef.current = init();

    console.log('EndUserRef', EndUserRef);

    EuSignRef.current = new EndUserRef.current(
      SIGN_WIDGET_PARENT_ID,
      SIGN_WIDGET_ID,
      SIGN_WIDGET_URI,
      EndUserRef.current.FormType.ReadPKey,
    );

    console.log('EuSignRef.current', EuSignRef.current);

    //= ============================================================================


    // Створення об'єкту типу endUser для взаємодії з iframe,
    // який завантажує сторінку SignWidget


    EuSignRef.current.ReadPrivateKey()
      .then((res) => {
        document.getElementById('sign-widget-parent').style.display = 'none';
        document.getElementById('sign-data-block').style.display = 'block';
        // console.log('res', res);
        // if (res.length > 0) {
        //   if (res[0].ownProperties('infoEx')) {
        //     console.log('res[0]', res[0]);
        //     dispatch(logicaActions.readPrivateKey(res[0].infoEx));
        //   }
        // }
      })
      .catch((e) => {
        console.log('!!!!!!!!!!!!! Виникла помилка при зчитуванні ос. ключа. !!!!!!!!!!!!!!!!!!!!');
        alert(`${'Виникла помилка при зчитуванні ос. ключа. ' +
        'Опис помилки: '}${e.message || e}`);
      });
  }, []);


  //= ============================================================================
  // Накладання підпису на дані

  const onSign = (strSign) => {
    // const previousSign = null;

    const external = false;
    const asBase64String = true;
    const signAlgo = EndUserRef.current.SignAlgo.DSTU4145WithGOST34311;
    const signType = EndUserRef.current.SignType.CAdES_X_Long;

    EuSignRef.current.SignData(
      strSign, external,
      asBase64String, signAlgo, null, signType,
    )
      .then(sign =>
        // console.log('============= sign  dataSign==================', sign, dataSign);
        document.getElementById('textAreaSign1').value = sign)

      .catch((e) => {
        alert(`${'Виникла помилка при підписі даних. ' +
        'Опис помилки: '}${e.message || e}`);
      });

    EuSignRef.current.SignHash(
      strSign, asBase64String, signAlgo, signType, null
    )
      .then(sign =>
        // console.log('============= sign  dataSign==================', sign, dataSign);
        document.getElementById('textAreaSign').value = sign)

      .catch((e) => {
        alert(`${'Виникла помилка при підписі даних. ' +
        'Опис помилки: '}${e.message || e}`);
      });
  };

  const promiseOnSign = (strSign) => {

    const asBase64String = true;
    const signAlgo = EndUserRef.current.SignAlgo.DSTU4145WithGOST34311;
    const signType = EndUserRef.current.SignType.CAdES_X_Long;

     return EuSignRef.current.SignHash(strSign, asBase64String, signAlgo, signType, null);
  };

  const authClick = async () => {
    const payload = await logicaActions.getIdLogica();
    console.log(payload);
    if (payload !== null) {
      setDataSign(payload.stringToSign);
      const signBase64 = await promiseOnSign(payload.stringToSign);
      setSignBase(signBase64);
    }
  };

  const ticketClick = async () => {
    const responseRequestId = await apiLogica.authentication.getIdForSign();
    if (responseRequestId.ok) {
      const { requestId, stringToSign } = await responseRequestId.json();
      try {
        const signBase64 = await promiseOnSign(stringToSign);
        const body = {
          signBase64,
          userAgent: 'Test',
          requestId,
        };
          // userAgent: '_Logica v.2',

        const responseLogin = await apiLogica.authentication.backEnd(body);


        if (responseLogin.ok) {
          const answearLogin = await responseLogin.json();

          console.log('answearLogin', answearLogin);
        }
      } catch (e) {
        console.log(e);
        return null;
      }
    } else {
      console.log('response', responseRequestId);
      return null;
    }


    // dispatch(logicaActions.loginLogica(dataSign, signBase64));
  };

  return (
    <>
      <div id="sign-widget-parent" style={{ width: '700px', height: '500px' }} />

      {/* Батківський елемент для відображення меню з підпису даних */}

      <div id="sign-data-block" style={{ display: 'none' }}>
        Дані для підпису:
        <div>
          <Textarea
            id="textAreaData"
            style={{ width: '510px', height: '50px;' }}
            value={dataSign}
            onChange={(e, value) => setDataSign(value)}
          />
        </div>
        <div>
          Підпис\Підписані дані:
        </div>
        <div>

          <Textarea
            id="textAreaSign"
            style={{ width: '510px', height: '400px;' }}
            value={signBase}
          />
        </div>
        <p>

          <Textarea
            id="textAreaSign1"
            style={{ width: '510px', height: '400px;' }}
          />
        </p>
      {/*  <div>*/}
      {/*    <Button*/}

      {/*      onClick={() => {*/}
      {/*        onSign(dataSign);*/}
      {/*      }}*/}
      {/*    >*/}
      {/*Підписати*/}
      {/*    </Button>*/}
      {/*  </div>*/}

        <Button
          onClick={() => authClick()}
        >
           Get ID for sing
        </Button>

        <Button
          onClick={() => ticketClick()}
        >
           Авторізація !
        </Button>
      </div>


    </>
  );

  // Очікування зчитування ос. ключа користувачем
};

// apiLogica.authentication.getIdForSign(dispatch);
//
//   if (response.ok) {
//     const data = await response.json();
//
//     dispatch(logicaActions.getIdLogica(data));
//     console.log('data', data);
//   } else {
//     console.log('response', response);
//   }
// }
// }}

// const mapState = (store) => {
//   const authNode = store.get('auth', new Map());
//
//   return {
//     privateKey: authNode.get('privateKey', new Map()),
//     logica: authNode.get('logica', new Map()),
//   };
// };
//
// //= ============================================================================
//
//
// export default connect(mapState)(Widget);
//
export default Widget;
