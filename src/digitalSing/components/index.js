import styled from 'styled-components';

export const Textarea = styled.textarea`
  // border: 1px solid rgba(34,36,38,.15);
  border: 1px solid #afbbcc;
  padding: 9.4px;
  border-radius: 4px;
  width: 100%;
  outline: none;
  resize: vertical;
`;

export const Button = styled.button`
  background: linear-gradient(0deg,#bfd4ef, #d1e6fb, #dfeefd,#c7daf2);
  // background: linear-gradient(0deg,#c9d7e8,#d5e4f7);
  // background: linear-gradient(0deg, #E3ECF7, #E3ECF7);
  border-radius: 4px;
  border:none;
  outline: none;
  cursor: ${({ disabled }) => (disabled ? 'unset' : 'pointer')};
  padding: 5px 8px;
  margin-right: 5px;
  font-weight: 700;
  display: inline-flex;
  align-items: center;
  filter: ${({ disabled }) => (disabled ? 'grayscale(1) opacity(.6)' : 'unset')};
  &:hover{
    background: #AAC6E6;
   };
   :focus {outline:none;}
`;

